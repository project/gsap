/**
 * @file
 * Global GSAP animations.
 */
((drupalSettings, gsap, ScrollTrigger) => {
  gsap.registerPlugin(ScrollTrigger);

  // Timeline.
  const tl = gsap.timeline({ paused: true })

  // Get animation entities.
  const animations = drupalSettings.gsap.global || [];
  animations.forEach((animation) => {
    const element = document.querySelector(animation.selector);
    if (element) {
      let json = animation.json;

      // Add in the scrollTrigger if it's a scrollTrigger animation.
      if (animation.event === 'scrollTrigger') {
        const stSettings = animation.scrolltrigger;
        json.scrollTrigger = {};

        // Debug.
        if (stSettings.markers) {
          json.scrollTrigger.markers = true;
        }

        // Trigger must exist.
        if (stSettings.trigger !== '' && document.querySelector(stSettings.trigger)) {
          json.scrollTrigger.trigger = stSettings.trigger;
        }

        // Start.
        if (stSettings.start !== '') {
          json.scrollTrigger.start = stSettings.start;
        }

        // End.
        if (stSettings.end !== '') {
          json.scrollTrigger.end = stSettings.end;
        }

        // Scrub can be true or a number.
        if (stSettings.scrub !== '') {
          if (!isNaN(stSettings.scrub)) {
            json.scrollTrigger.scrub = stSettings.scrub;
          }
          else {
            json.scrollTrigger.scrub = true;
          }
        }

        // Pin can be true or a selector.
        if (animation.pin !== '') {
          if (document.querySelector(animation.pin)) {
            json.scrollTrigger.pin = animation.pin;
          }
          else {
            json.scrollTrigger.pin = true;
          }
        }
      }
      // Click and hover are paused to start.
      else {
        json.paused = true;
      }

      // Create the tween.
      const tween = gsap[animation.direction](animation.selector, json);

      // Click events.
      if (animation.event === 'click') {
        element.addEventListener('click', () => {
          tween.play();
        });
      }
      // Hover events.
      else if (animation.event === 'hover') {
        element.addEventListener('mouseenter', () => {
          tween.play();
        });
        element.addEventListener('mouseleave', () => {
          tween.reverse();
        });
      }
    }
  });

})(drupalSettings, gsap, ScrollTrigger);
