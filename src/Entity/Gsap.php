<?php

namespace Drupal\gsap\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\gsap\GsapInterface;

/**
 * Defines the gsap entity type.
 *
 * @ConfigEntityType(
 *   id = "gsap",
 *   label = @Translation("GSAP"),
 *   label_collection = @Translation("GSAPs"),
 *   label_singular = @Translation("gsap"),
 *   label_plural = @Translation("gsaps"),
 *   label_count = @PluralTranslation(
 *     singular = "@count gsap",
 *     plural = "@count gsaps",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\gsap\GsapListBuilder",
 *     "form" = {
 *       "add" = "Drupal\gsap\Form\GsapForm",
 *       "edit" = "Drupal\gsap\Form\GsapForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "gsap",
 *   admin_permission = "administer gsap",
 *   links = {
 *     "collection" = "/admin/structure/gsap",
 *     "add-form" = "/admin/structure/gsap/add",
 *     "edit-form" = "/admin/structure/gsap/{gsap}",
 *     "delete-form" = "/admin/structure/gsap/{gsap}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "event",
 *     "scrolltrigger",
 *     "selector",
 *     "direction",
 *     "json"
 *   },
 * )
 */
final class Gsap extends ConfigEntityBase implements GsapInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $label;

  /**
   * The example description.
   */
  protected string $description;

}
