<?php

namespace Drupal\gsap;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a gsap entity type.
 */
interface GsapInterface extends ConfigEntityInterface {

}
