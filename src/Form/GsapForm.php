<?php

namespace Drupal\gsap\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gsap\Entity\Gsap;
use Symfony\Component\Yaml\Yaml;

/**
 * GSAP form.
 */
final class GsapForm extends EntityForm {

  /**
   * The allowed GSAP properties.
   *
   * @var string[]
   */
  const ALLOWED_GSAP_PROPS = [
    'delay',
    'duration',
    'ease',
    'id',
    'yoyo',
    'x',
    'y',
    'scale',
    'scaleY',
    'scaleX',
    'rotation',
    'skewX',
    'skewY',
  ];

  /**
   * The allowed CSS properties.
   *
   * @var string[]
   */
  const ALLOWED_CSS_PROPS = [
    'backgroundColor',
    'color',
    'fontSize',
    'height',
    'left',
    'margin',
    'opacity',
    'padding',
    'right',
    'top',
    'transform',
    'width',
  ];

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['warning'] = [
      '#type' => 'item',
      '#markup' => $this->t('<strong>NOTE:</strong> By enabling any GSAP animations, both gsap and scrollTrigger will be enabled on all pages!.'),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [Gsap::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['event'] = [
      '#type' => 'select',
      '#title' => $this->t('Event Trigger'),
      '#default_value' => $this->entity->get('event') ?? 'click',
      '#options' => [
        'click' => $this->t('Click'),
        'hover' => $this->t('Hover'),
        'scrollTrigger' => $this->t('ScrollTrigger'),
      ],
      '#required' => TRUE,
    ];

    $form['scrolltrigger'] = [
      '#title' => $this->t('Scroll Trigger Settings'),
      '#type' => 'details',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="event"]' => ['value' => 'scrollTrigger'],
        ],
      ],
    ];

    $scrolltrigger = $this->entity->get('scrolltrigger') ?? [];
    $form['scrolltrigger']['markers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug Markers'),
      '#default_value' => $scrolltrigger['markers'] ?? FALSE,
    ];

    $form['scrolltrigger']['trigger'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trigger Selector'),
      '#description' => $this->t('The selector for scroll trigger or blank for self.'),
      '#default_value' => $scrolltrigger['trigger'] ?? FALSE,
    ];

    $form['scrolltrigger']['start'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Value'),
      '#description' => $this->t('Leave blank for default.'),
      '#default_value' => $scrolltrigger['start'] ?? FALSE,
    ];

    $form['scrolltrigger']['end'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End Value'),
      '#description' => $this->t('Leave blank for default.'),
      '#default_value' => $scrolltrigger['end'] ?? FALSE,
    ];

    $form['scrolltrigger']['scrub'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scrub'),
      '#description' => $this->t('The scrub value or "true", leave blank to not scrub.'),
      '#default_value' => $scrolltrigger['scrub'] ?? FALSE,
    ];

    $form['scrolltrigger']['pin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pin Selector'),
      '#description' => $this->t('The selector to pin the element to or "true", leave blank to not pin.'),
      '#default_value' => $scrolltrigger['pin'] ?? '',
    ];

    $form['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selector'),
      '#default_value' => $this->entity->get('selector'),
      '#required' => TRUE,
    ];

    $form['direction'] = [
      '#type' => 'select',
      '#title' => $this->t('Direction'),
      '#options' => [
        'to' => $this->t('To'),
        'from' => $this->t('From'),
      ],
      '#default_value' => $this->entity->get('direction') ?? 'to',
      '#required' => TRUE,
    ];

    $form['json'] = [
      '#type' => 'textarea',
      '#title' => $this->t('JSON'),
      '#description' => $this->t('Paste the JSON string for your gsap. Example: {x: 100, y: 100, ease: "power1.inOut"}'),
      '#default_value' => $this->entity->get('json'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);
    $pastedJson = $form_state->getValue('json');
    if (!empty($pastedJson)) {
      try {
        $json = Yaml::parse(trim($pastedJson));
      }
      catch (\Exception $e) {
        $json = NULL;
      }
      if ($json === NULL) {
        $form_state->setErrorByName('json', $this->t('The pasted value must be valid JSON.'));
      }
      else {
        foreach ($json as $key => $value) {
          if (!in_array($key, array_merge(self::ALLOWED_GSAP_PROPS, self::ALLOWED_CSS_PROPS))) {
            $form_state->setErrorByName('json', $this->t('The key %key is not allowed.', ['%key' => $key]));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match ($result) {
        \SAVED_NEW => $this->t('Created new Gsap %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated Gsap %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
