<?php

namespace Drupal\gsap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Admin settings for GSAP.
 *
 * @package Drupal\gsap\Form
 */
class Admin extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gsap.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gsap_admin';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gsap.settings');
    $form = parent::buildForm($form, $form_state);

    $form['include_gsap'] = [
      '#title' => $this->t('Include GSAP on every page?'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('include_gsap') ?? FALSE,
    ];

    $form['include_libs'] = [
      '#title' => $this->t('Include additional libs on every page?'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('include_libs') ?? FALSE,
      '#states' => [
        'disabled' => [
          ':input[name="include_gsap"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['libs'] = [
      '#title' => $this->t('Available CDN Libraries'),
      '#description' => $this->t('Select the plugins to include as libraries. These will be added to every page.'),
      '#type' => 'checkboxes',
      '#default_value' => $config->get('libs') ?? [],
      '#options' => [
        'flip' => $this->t('Flip'),
        'scrolltrigger' => $this->t('ScrollTrigger'),
        'observer' => $this->t('Observer'),
        'scrollto' => $this->t('ScrollTo'),
        'draggable' => $this->t('Draggable'),
        'easel' => $this->t('Easel'),
        'motionpath' => $this->t('MotionPath'),
        'pixi' => $this->t('Pixi'),
        'text' => $this->t('Text'),
      ],
      '#states' => [
        'invisible' => [
          ':input[name="include_libs"]' => ['enabled' => FALSE],
        ],
        'disabled' => [
          ':input[name="include_libs"]' => ['checked' => FALSE],
        ],
      ],
    ];

    // Keyword detection settings.
    $key_values = $config->get('custom_libs');
    if ($form_state->get('key_values') === NULL) {
      $form_state->set('key_values', is_array($key_values) ? $key_values : []);
    }
    $form['custom_libs'] = [
      '#type' => 'details',
      '#title' => $this->t('Additional Libraries'),
      '#description' => $this->t("The name will be created as a library of 'gsap/[key]'. These will NOT be added to the page automatically."),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#attributes' => ['id' => 'edit-custom'],
    ];
    foreach ($form_state->get('key_values') as $delta => $key_value) {
      $form['custom_libs'][$delta] = [
        '#type' => 'details',
        '#title' => $this->t('Name: @key', [
          '@key' => isset($key_values[$delta]) ? key($key_values[$delta]) : $this->t('**Not yet saved**'),
        ]),
        '#open' => TRUE,
      ];
      $form['custom_libs'][$delta]['key'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Library Key'),
        '#description' => $this->t("Will be added as gsap/[key]"),
        '#element_validate' => ['::validateLibKey'],
        '#default_value' => isset($key_values[$delta]) ? key($key_values[$delta]) : NULL,
      ];
      $form['custom_libs'][$delta]['value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Path to library'),
        '#description' => $this->t('Either URL or absolute path from docroot.'),
        '#default_value' => isset($key_values[$delta]) ? reset($key_values[$delta]) : NULL,
      ];
      $form['custom_libs'][$delta]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => "remove_$delta",
        '#submit' => [[$this, 'removeLib']],
        '#ajax' => [
          'callback' => [$this, 'ajaxLibs'],
          'wrapper' => 'edit-custom',
        ],
        '#attributes' => ['class' => ['field-add-more-submit']],
      ];
    }
    $form['custom_libs']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add new library'),
      '#submit' => [[$this, 'addLib']],
      '#ajax' => [
        'callback' => [$this, 'ajaxLibs'],
        'wrapper' => 'edit-custom',
      ],
      '#attributes' => ['class' => ['field-add-more-submit']],
    ];

    return $form;
  }

  /**
   * Validates lib keys.
   */
  public static function validateLibKey($element, &$form_state) {
    $value = $element['#value'];
    if (!preg_match('/^[a-z_]+$/', $value)) {
      $form_state->setError($element, t('Please enter lower case letters only and no spaces.'));
    }
  }

  /**
   * Submit for libs.
   */
  public function ajaxLibs(array &$form, FormStateInterface $form_state) {
    return $form['custom_libs'];
  }

  /**
   * Remove lib callback.
   */
  public function removeLib(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $delta = $form_state->getTriggeringElement()['#array_parents'][1];
    $key_values = $form_state->get('key_values');
    unset($key_values[$delta]);
    $form_state->set('key_values', $key_values);
  }

  /**
   * Add lib callback.
   */
  public function addLib(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    $key_values = $form_state->get('key_values');
    $key_values[] = [];
    $form_state->set('key_values', $key_values);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $libraries = array_values(array_filter($form_state->getValue('libs')));

    $custom = $form_state->cleanValues()->getValue('custom_libs');
    $custom_libs = [];
    foreach ($custom as $lib) {
      if (!empty($lib['key']) && !empty($lib['value'])) {
        $custom_libs[] = [$lib['key'] => $lib['value']];
      }
    }

    $this->configFactory->getEditable('gsap.settings')
      ->set('libs', $libraries)
      ->set('include_gsap', $form_state->getValue('include_gsap'))
      ->set('include_libs', $form_state->getValue('include_libs'))
      ->set('custom_libs', $custom_libs)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
